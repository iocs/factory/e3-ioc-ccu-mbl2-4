require essioc
require iocmetadata
require rfsccu,

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Device
epicsEnvSet(P, "MBL-020RFC:")
epicsEnvSet(R, "RFS-CCU-420:")

# MOXA
epicsEnvSet(MHOST, "mbl2-rf4-moxa.tn.esss.lu.se:4004")

iocshLoad("$(rfsccu_DIR)rfsccu.iocsh", "IPP=$(MHOST), P=$(P), R=$(R)")

pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")

